---
title: Testimonials
menu: Citát
visible: false
testimonials:
    -
        author: 'Neznámy autor'
        content: 'Naše srdcia sú čierne diery nespokojnosti'
    -
        author: 'Marián Palko'
        content: 'Nehádžte ľuďom polená pod nohy. Nechajte si ich na zimu!'
---

