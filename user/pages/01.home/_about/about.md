---
title: about
menu: O Tábore
address:
    - line: Richard Nagypál
    - line: Muránska Zdychava (Karafová)
    - line: +421 911 868 811
email:
    - address: richard.nagypal@gmail.com
<!-- buttons:
    - url: "#"
      icon: download
      text: Download Resume -->
---
## O Tábore

Všetci ľudia hľadajú šťastie. Bez výnimky. Nech je to akýmkoľvek spôsobom, každému ide o to, aby ho našiel. Túžba, prečo niektorí vyvolávajú vojnu a iní ju potláčajú, je rovnaká. Toto je motív každého skutku každého človeka, aj samovrahov. Každý túži byť uspokojený a naplnený a každý urobí všetko, čo je v jeho silách, aby to našiel. V našich srdciach je však priepasť. A je veľsmi hlboká.
