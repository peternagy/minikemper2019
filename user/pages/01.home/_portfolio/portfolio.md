---
title: portfolio
menu: Fotky
portfolio:
  - title: "Minikemper"
    description: Spoločnosť
    img: 1.jpg
    content: ""
    tags:
    details:
  - title: "Minikemper"
    description: Hry
    img: 2.jpg
    content: ""
    tags:
    details:
  - title: "Minikemper"
    description: Príroda
    img: 3.jpg
    content: "."
    tags: Branding
    details:
  - title: "Minikemper"
    description: Výzvy
    img: 4.jpg
    content: ""
    tags:    
    details:
---
#Fotografie z minulých ročníkov.
