---
title: resume
menu: Program
sections: 
    - title: Info
      css_class: education
      items:
        - title: Mládežnícka chata
          info: Muránska Zdychava
          date: 2.-5.1.2019
          description:  Po spoločne strávenom lete na Megakemperi nás čaká zimná chatovačka Minikemper. Ak máš 15-25 rokov, tak si zbaľ kufre, teplé oblečenie a príď s nami zažiť kus divočiny. Kemp organizujú mladí kresťania z Revúcej a okolia, nadšení z mladých ľudí a pre mladých ľudí. Budeš počuť o tom, čomu a komu veríme, spoločne budeme premýšľať a diskutovať o tom, či nám niečo dôležité nechýba. Pokúsime sa zistiť, aké nebezpečné sú priepasti v našich životoch.
    - title: Nezabudni si
      css_class: skill
      items:
        - title:
          info:
          date:
          description: Spacák, karimatku, stan, teplé oblečenie, pevné topánky do lesa, pršiplášť, ponožky (veľa ponožiek), čapica, šál (koberec alebo deka), hygienické potreby, baterka/čelovka, zápisník, pero, lieky (v prípade ak nejaké užívaš),  deku, kartu o zdravotnom poistení (nie kópiu).  
          <!-- skills:
            - name: Photoshop
              level: 60
            - name: Illustrator
              level: 55
            - name: Wordpress
              level: 50
            - name: CSS
              level: 90
            - name: Html5
              level: 80
            - name: Jquery
              level: 50             -->
---
