---
title: Minikemper
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _about
            - _resume
            - _portfolio
            - _testimonials
            - _contact
menu: Domov
form:
    name: miniregistration
    action:
    fields:
        -
            name: name
            label: Meno a Priezvisko
            placeholder: 'Zadajte svoje meno a priezvisko'
            autocomplete: 'on'
            type: text
            validate:
                required: true
        -
            name: email
            label: Email
            placeholder: 'Zadajte svoju emailovú adresu'
            type: text
            validate:
                rule: email
                required: true
        - name: birthdate
          label: Dátum narodenia
          placeholder: 'Zadajte dátum narodenia'
          autocomplete: on
          type: text
          validate:
            required: true
        - name: address
          label: Adresa
          placeholder: 'Zadajte adresu trvalého bydliska'
          autocomplete: on
          type: text
          validate:
            required: true
        - name: number
          label: Telefón
          placeholder: 'Zadajte telefónne číslo'
          autocomplete: on
          type: text
          validate:
            required: true
        -
            name: message
            label: Poznámka
            size: long
            placeholder: 'V prípade potreby zadajte poznámku'
            type: textarea
    buttons:
        -
            type: submit
            value: Registruj
            class: submit
    process:
        -
            email:
                from: "{{ config.plugins.email.from }}"
                to: "{{ form.value.email }}"
                subject: "[Registácia] {{ form.value.name|e }}"
                body: "Milá/ý {{ form.value.name|e }},<br/><br/>Ďakujeme za registráciu na Minikemper 2019.<br/><br/>Platobné údaje:<br/>IBAN: SK6609000000000511952302<br/>Suma: 15€<br/>Správa pre príjemcu: {{ form.value.name|e }}<br/><br/>Tešíme sa, že budeme môcť s Tebou stráviť pár dní.<br/>Organizačný team Minikemper 2019"
        -
            email:
                from: '{{ config.plugins.email.from }}'
                to:
                    - '{{ config.plugins.email.to }}'
                    - '{{ form.value.email }}'
                subject: '[Prihlasovanie-Minikemper2019] {{ form.value.name|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            changefile:
        -
            save:
                fileprefix: minikemper-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: 'Thank you for your feedback!'
        -
            display: /
onpage_menu: true
---
