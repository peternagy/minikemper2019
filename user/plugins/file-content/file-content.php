<?php
namespace Grav\Plugin;

use Grav\Common\Plugin;
use RocketTheme\Toolbox\Event\Event;

class FileContentPlugin extends Plugin
{

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0],
            'onFormProcessed' => ['onFormProcessed', 0]
        ];
    }

    /**
     * Enable search only if url matches to the configuration.
     */
    public function onPluginsInitialized()
    {
        if ($this->isAdmin()) {
            $this->active = false;
            return;
        }

        $this->enable([
            'onTwigExtensions' => ['onTwigExtensions', 0]
        ]);

    }

    public function onFormProcessed(Event $event)
    {
        $form = $event['form'];
        $action = $event['action'];
        $params = $event['params'];

        switch ($action) {
            case 'changefile':
                $myfile = fopen(__DIR__."/free.txt", "r") or die("Unable to open file!");
                $freeplaces = fgets($myfile);
                fclose($myfile);

                $freeplaces = $freeplaces - 1;

                $myfile = fopen(__DIR__."/free.txt", "w") or die("Unable to open file!");
                fwrite($myfile, $freeplaces);
                fclose($myfile);
        }
    }

    /**
     * Add Twig Extensions
     */
    public function onTwigExtensions()
    {
        require_once(__DIR__.'/twig/FileContentTwigExtension.php');
        $this->grav['twig']->twig->addExtension(new FileContentTwigExtension());
    }
}
